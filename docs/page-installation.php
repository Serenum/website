<?php

	require_once 'site-header.php';







	echo '<section id="installation">';
		echo '<h1>Installation</h1>';
		echo '<p>To install Serenum, you need to the following steps. The installation will be improved over time.</p>';

		echo '<ol>';
			echo '<li>Download/clone the source code from '.link_('the repository', 'https://codeberg.org/serenum/website').'.</li>';
			echo '<li>Extract or move the files to your web server.</li>';
			echo '<li>';
				echo 'If you wish to enable database support, change <code>save_to_db->enabled</code> in <code>config.json</code> to <code>true</code>.';
				echo '<ol>';
					echo '<li>Enter your database information in the variables that starts with <code>$db_</code> in <code>site-settings.php</code>.</li>';
					echo '<li>Enter the private encryption keys in the <code>$settings_encryption_key</code> and <code>$settings_encryption_iv</code> variables. If these variables are empty, Serenum will not be able to save the data to the database.</li>';
					echo '<li>Go to <code>/create-db</code> and follow the instuctions.</li>';
				echo '</ol>';
			echo '</li>';
			echo '<li>Edit the settings after your likings in <code>config.json</code>.</li>';
			echo '<li>Enter your API key from OpenWeatherMap in <code>defaults.json</code>.</li>';
			echo '<li>Move <code>defaults.json</code> outside of the root folder.</li>';
			echo '<li>You\'re all done! Now, go to the address you have for Serenum and enjoy :)</li>';
		echo '</ol>';


		echo '<h3>Automatically delete settings</h3>';
		echo '<p>To let Serenum delete the saved settings that are 1 month old or older, add the following to your crontab:</p>';
		echo '<pre><code>0 0 1 * * /usr/bin/php /path/to/crontab-delete.php</code></pre>';
	echo '</section>';







	require_once 'site-footer.php';

?>
