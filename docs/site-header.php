<?php

	require_once 'site-settings.php';



	echo '<!DOCTYPE html>';
	echo '<html>';
		echo '<head>';
			echo '<title>';
				echo $og_title.' Docs';
			echo '</title>';

			echo '<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">';
			echo '<meta name="robots" content="noindex, nofollow">';
			echo '<meta name="google" content="notranslate">';

			echo '<meta property="og:title" content="'.$og_title.'">';
			echo '<meta property="og:site_name" content="'.$og_title.'">';
			echo '<meta property="og:type" content="website">';
			echo '<meta property="og:url" content="'.(isset($og_url) ? '-' : $og_url).'">';
			echo '<meta property="og:description" content="'.($og_description == null ? '-' : $og_description).'">';
			echo '<meta property="og:locale" content="sv_SE">';
			echo '<meta http-equiv="refresh" content="3600">';

			echo '<meta name="twitter:card" content="summary"></meta>';

			echo '<link rel="canonical" href="'.(isset($og_url) ? '-' : $og_url).'">';
			echo '<link href="'.(isset($og_url) ? '-' : $og_url).'" rel="me">';

			echo '<link href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQEAYAAABPYyMiAAAABmJLR0T///////8JWPfcAAAACXBIWXMAAABIAAAASABGyWs+AAAAF0lEQVRIx2NgGAWjYBSMglEwCkbBSAcACBAAAeaR9cIAAAAASUVORK5CYII=" rel="icon" type="image/x-icon">';
			echo '<link rel="dns-prefetch" href="https://cdn.serenum.org" crossorigin="">';

			#echo '<link type="text/css" rel="stylesheet" href="https://cdn.serenum.org/fonts.php?victor+mono:400,700">';
			echo '<link type="text/css" rel="stylesheet" href="'.url('desktop.css?'.time()).'">';
			echo '<link type="text/css" rel="stylesheet" href="'.url('portable.css?'.time()).'">';
		echo '</head>';
		echo '<body>';



			echo '<section id="website">';
				echo '<header id="logo">';
					echo '<a href="'.url('', false).'">';
						echo $config->title.' Docs';
					echo '</a>';
				echo '</header>';

				echo '<nav id="main">';
					echo '<a href="'.url('').'">';
						echo 'Introduction';
					echo '</a>';

					echo '<a href="'.url('installation').'">';
						echo 'Installation';
					echo '</a>';

					echo '<a href="'.url('api').'">';
						echo 'API';
					echo '</a>';
				echo '</nav>';

				echo '<main>';

?>
