<?php

	require_once '../site-settings.php';



	$post_coordinates = (empty($_POST['field-coordinates']) ? null : str_replace('/', ',', strip_tags(htmlspecialchars($_POST['field-coordinates']))));
	$post_apikey = (!empty($_POST['field-apikey-owm']) ? strip_tags(htmlspecialchars($_POST['field-apikey-owm'])) : 'default');
	$post_language = (isset($_POST['list-languages']) ? strip_tags(htmlspecialchars($_POST['list-languages'])) : $get_language);
	$post_temperature = (isset($_POST['temperature']) ? strip_tags(htmlspecialchars($_POST['temperature'])) : $get_temperature);
	$post_windspeed = (isset($_POST['windspeed']) ? strip_tags(htmlspecialchars($_POST['windspeed'])) : $get_windspeed);
	$post_pressure = (isset($_POST['pressure']) ? strip_tags(htmlspecialchars($_POST['pressure'])) : $get_pressure);
	$post_dateformat = (isset($_POST['dateformat']) ? strip_tags(htmlspecialchars($_POST['dateformat'])) : $get_dateformat);
	$post_timeformat = (isset($_POST['timeformat']) ? strip_tags(htmlspecialchars($_POST['timeformat'])) : $get_timeformat);
	$post_daysuffix = (isset($_POST['daysuffix']) ? strip_tags(htmlspecialchars($_POST['daysuffix'])) : $get_daysuffix);
	$post_distance = (isset($_POST['distance']) ? strip_tags(htmlspecialchars($_POST['distance'])) : $get_distance);
	$post_precipitation = (isset($_POST['precipitation']) ? strip_tags(htmlspecialchars($_POST['precipitation'])) : $get_precipitation);
	$post_decimal = (isset($_POST['decimal']) ? strip_tags(htmlspecialchars($_POST['decimal'])) : $get_decimal);
	$post_splitthousands = (isset($_POST['split-thousands']) ? strip_tags(htmlspecialchars($_POST['split-thousands'])) : $get_splitthousands);
	$post_theme = (isset($_POST['theme']) ? strip_tags(htmlspecialchars($_POST['theme'])) : $get_theme);

	if($post_coordinates != null) {
		list($latitude, $longitude) = explode(',', $post_coordinates);
	}

	$coordinates = '?coo='.(empty($post_coordinates) ? null : format_number($latitude, 3, '.').','.format_number($longitude, 3, '.'));
	$language = '&lan='.$post_language;
	$temperature = '&tem='.$post_temperature;
	$windspeed = '&wis='.$post_windspeed;
	$pressure = '&prs='.$post_pressure;
	$dateformat = '&dat='.$post_dateformat;
	$timeformat = '&tim='.$post_timeformat;
	$daysuffix = '&dsu='.$post_daysuffix;
	$distance = '&dis='.$post_distance;
	$precipitation = '&prc='.$post_precipitation;
	$decimal = '&dec='.$post_decimal;
	$split_thousands = '&ths='.$post_splitthousands;
	$theme = '&thm='.$post_theme;
	$apikey = '&api='.$post_apikey;

	$string = $coordinates . $language . $temperature . $windspeed . $pressure . $dateformat . $timeformat . $daysuffix . $distance . $precipitation . $decimal . $split_thousands . $theme . $apikey;

	logger('Changed the options to the following: '.$post_language.' '.$post_temperature . $post_windspeed . $post_pressure . $post_dateformat . $post_timeformat . $post_daysuffix . $post_distance . $post_precipitation . $post_decimal . $post_splitthousands . $post_theme, 'debug');


	header("Location: ".url('weather'.$string, false));
	exit;

?>