{
	"metadata": {
		"name": "Svenska",
		"description": "Serenum är en vädertjänst som är mån om din integritet."
	},
	"titles": {
		"place": "Plats",
		"location_settings": "Ändra plats & inställningar",
		"settings": "Inställningar",
		"apikeys": "API-nycklar",
		"weather": {
			"report": "Väderleksrapport",
			"forecasts": {
				"minutely": "Minutsvis nederbördsprognos i mm",
				"hourly": "48-timmarsprognos",
				"daily": "7-dagarsprognos"
			}
		}
	},
	"menu": {
		"now": "Nu",
		"hourly": "48 timmar",
		"daily": "7 dagar",
		"astronomy": "Astronomi"
	},
	"footer": {
		"poweredby": "Drivs av",
		"page_autorefresh": "Sidan laddas automatiskt om en gång per timme.",
		"nav": {
			"weather": "Vädret",
			"intro": "Intro/Om",
			"save": "Spara",
			"restore": "Återställ",
			"contact": "Kontakta",
			"docs": "Dokumentation"
		}
	},
	"placeholders": {
		"required": "Krävs"
	},
	"map": {
		"fullscreen": {
			"open": "Öppna kartan i fullskärmsläge",
			"close": "Stäng fullskärmsläget (Esc)"
		},
		"zoom": {
			"in": "Zooma in",
			"out": "Zooma ut"
		},
		"marker": {
			"click": "Klicka på kartan för att välja plats",
			"drag": "Dra markören för att välja plats"
		}
	},
	"words": {
		"temperature": {
			"actual": "Temperatur",
			"feels_like": "Känns som",
			"dew_point": "Daggpunkt",
			"day": "Temperatur (dag)",
			"morning": "Temperatur (morgon)",
			"evening": "Temperatur (kväll)",
			"night": "Temperatur (natt)"
		},
		"wind": {
			"speed": "Vindhastighet",
			"direction": "Vindriktning",
			"beaufort": "Beaufort"
		},
		"clouds": "Molntäcke",
		"humidity": "Luftfuktighet",
		"h2o_kg_air": "H<span class='sub'>2</span>O per kg av luft",
		"pressure": "Lufttryck",
		"uvindex": "UV-index",
		"visibility": "Sikt",
		"precipitation": {
			"rain": {
				"full": "Nederbörd (regn)",
				"short": "Nederbörd (regn)"
			},
			"snow": {
				"full": "Nederbörd (snö)",
				"short": "Nederbörd (snö)"
			}
		},
		"airquality": "Luftkvalité",
		"comma": "Komma",
		"dot": "Punkt",
		"grams": "Gram",
		"lowest": "Lägst",
		"highest": "Högst",
		"yes": "Ja",
		"no": "Nej",
		"download": "Ladda hem",
		"example": "Exempel",
		"save": "Spara",
		"restore": "Återställ",
		"sunrise": "Soluppgång",
		"sunset": "Solnedgång",
		"password": "Lösenord",
		"auto": "Auto",
		"light": "Ljust",
		"dark": "Mörkt",
		"days": {
			"monday": "Måndag",
			"tuesday": "Tisdag",
			"wednesday": "Onsdag",
			"thursday": "Torsdag",
			"friday": "Fredag",
			"saturday": "Lördag",
			"sunday": "Söndag",
			"humanreadable": {
				"today": "I dag",
				"day1_ahead": "I morgon"
			}
		},
		"months": {
			"january": {
				"full": "Januari",
				"short": "Jan"
			},
			"february": {
				"full": "Februari",
				"short": "Feb"
			},
			"march": {
				"full": "Mars",
				"short": "Mar"
			},
			"april": {
				"full": "April",
				"short": "Apr"
			},
			"may": {
				"full": "Maj",
				"short": "May"
			},
			"june": {
				"full": "Juni",
				"short": "Jun"
			},
			"july": {
				"full": "Juli",
				"short": "Jul"
			},
			"august": {
				"full": "Augusti",
				"short": "Aug"
			},
			"september": {
				"full": "September",
				"short": "Sep"
			},
			"october": {
				"full": "Oktober",
				"short": "Okt"
			},
			"november": {
				"full": "November",
				"short": "Nov"
			},
			"december": {
				"full": "December",
				"short": "Dec"
			}
		}
	},
	"settings": {
		"place": {
			"description": "Om du föredrar att välja en plats på en karta, använd kartan nean."
		},
		"gps": {
			"start": "Hämta min nuvarande position",
			"found": "Hämta min nuvarande position igen",
			"fetching": "Försöker att positionera dig...",
			"errors": {
				"permission_denied": "Positioneringen nekades",
				"position_unavailable": "Positioneringen är inte tillgänglig",
				"timeout": "Positioneringens tidsgräns löpte ut"
			},
			"desc": "Din nuvarande plats kommer aldrig att delas till Serenum.",
			"try_again": "Försök igen"
		},
		"coordinates": "Koordinater",
		"options": {
			"place": "Plats",
			"language": "Språk",
			"temperature": "Temperatur",
			"windspeed": "Vindhastighet",
			"pressure": "Lufttryck",
			"daysuffix": {
				"name": "Dagssuffix",
				"show": "Visa",
				"hide": "Dölj"
			},
			"dateformat": "Datumformat",
			"timeformat": "Tidsformat",
			"distance": "Distans",
			"precipitation": "Nederbörd",
			"decimal": "Decimaltecken",
			"split_thousands": "Dela tusental",
			"theme": "Tema"
		},
		"apikeys": {
			"owm": "OpenWeatherMap API-nyckel"
		},
		"placeholders": {
			"place": "Example: Karlstad,SE or coordinates"
		},
		"button_save": "Spara mina val"
	},
	"weather": {
		"sunny": "Klart",
		"moon": "Klart",
		"clouds": {
			"few": "Fåtal moln",
			"scattered": "Spridda moln",
			"broken": "Delvis molnighet",
			"overcast": "Mulet"
		},
		"drizzle": {
			"light_intensity": "Lätt regn med låg intensitet",
			"drizzle": "Duggregn",
			"heavy_intensity": "Kraftigt regn med hög intensitet",
			"light_intensity_rain": "Lätt regn med låg intensitet",
			"rain": "Regn & duggregn",
			"heavy_intensity_rain": "Kraftigt regn med hög intensitet",
			"shower_rain": "Regnskurar & duggregn",
			"heavy_shower_rain": "Kraftigt regnskurar & duggregn",
			"shower": "Regnskurar"
		},
		"rain": {
			"light_intensity_shower": "Lätt regnskur",
			"shower": "Regnskurar",
			"heavy_intensity_shower": "Mycket regnskurar",
			"ragged_shower": "Trasigt duschregn",
			"light": "Lätt regn",
			"moderate": "Måttligt regn",
			"heavy_intensity": "Kraftigt regn",
			"very_heavy": "Mycket kraftigt regn",
			"extreme": "Skyfall"
		},
		"thunderstorm": {
			"light_rain": "Åskoväder med lätt regn",
			"rain": "Åskoväder med regn",
			"heavy_rain": "Åskoväder med mycket regn",
			"light": "Lätt åskoväder",
			"thunderstorm": "Åskoväder",
			"heavy": "Kraftigt åskoväder",
			"ragged": "Trasigt åskoväder",
			"light_drizzle": "Åskoväder med lätt duggregn",
			"drizzle": "Åskoväder med duggregn",
			"heavy_drizzle": "Åskoväder med mycket duggregn"
		}
	},
	"messages": {
		"no_coordinates": "Vänligen ange koordinaterna.",
		"notvalid_coordinates": "Vänligen ange giltiga koordinater.",
		"notvalid_apikey": "Vänligen ange en giltig API-nyckel för OpenWeatherMap.",
		"info_null": "Kunde inte hämta information.",
		"unknown_place": "Okänd plats",
		"open_in_osm": "Öppna i OpenStreetMap",
		"save_settings": {
			"nothing_provided": "Vänligen välj dina inställningar först."
		},
		"errors": {
			"no_password": "Vänligen ange ett lösenord för att fortsätta.",
			"wrong_password": "Det angivna lösenordet är fel.",
			"no_file": "Vänligen ange en fil först.",
			"file_not_uploaded": "Filen kunde inte laddas upp.",
			"file_too_big": "Filstorleken av den valda filen är för stor.",
			"file_format_invalid": "Filformatet av den valda filen är inte giltig.",
			"file_not_json": "Filen innehåller ingen JSON."
		}
	},
	"pages": {
		"start": {
			"title": "Välkommen till Serenum",
			"content": [
				"Serenum är en svenskbaserad vädertjänst som låter dig ha kontrollen över din egna data och som anpassar sig efter dina val.",
				"### Funktioner",
				"- Många val av inställningar, bland annat kunna ange sin egna API-nyckel från OpenWeatherMap.\n- Minutvis prognos för en timme framåt.\n- 48-timmarsprognos.\n- 7-dagarsprognos.\n- Diagram för temperatur, vindhastighet, molntäcke, luftfuktighet och nederbörd för både regn och snö.\n- Möjligheten att spara de valda inställningarna (ej standard) krypterade till databasen eller i klartext till en fil.\n- Kunna återställa sparade inställningar.\n- Webbsidan laddas om automatiskt en gång per timme.",
				"### Öppen källkod och self-hosting",
				"Till skillnad från många andra vädertjänster, kan du se hur Serenum är byggd på insidan (källkoden). Om du vill och kan hjälpa till med att göra Serenum bättre, vänligen gör det.",
				"Om du har en egen webbserver så kan du installera Serenum och använda tjänsten antingen helt lokalt (utan CDN) eller delvis lokalt (med CDN).",
				"<div class='attention'>",
					"### Förvänta lång laddningstid",
					"Serenum är inte baserad på JavaScript. När du ska visa nuvarande rapport och prognoserna, förvänta dig minst 2 sekunders laddningstid.",
				"</div>",
				"<div class='warning'>",
					"### Serenum kan visa fel",
					"Precis som alla andra vädertjänster kan Serenum visa fel väderleksrapport eller väderleksprognos. Detta är ingen bugg utan ett tekniskt fel då vi inte har kommit så långt i tekniken än.",
					"Ta **allt** med nypa salt tills du har kunnat bekräfta att Serenum visar rätt eller fel. Detta gäller oavsett vilken vädertjänst du använder.",
				"</div>"
			],
			"button": "Visa vädret"
		},
		"settings": {
			"save": {
				"title": "Spara dina inställningarna",
				"database": {
					"content": [
						"### Spara i databasen",
						"Kopiera lösenordet som visas nedan och spara det på ett säkert ställe. När du har valt att spara inställningarna, kommer Serenum att lagra dom krypterade i databasen.",
						"**Vänligen notera** att om du inte har återställt inställningarna under 1 månads tid, kommer dom att raderas från databasen."
					]
				},
				"json": {
					"content": [
						"### Spara som en JSON-fil",
						"Du kan spara de valda inställningarna genom att ladda hem en JSON-fil. Den här metoden låter dig ändra dina inställningar utan internet och sen återställa dom när du har internet igen."
					]
				}
			},
			"restore": {
				"title": "Återställ dina inställningarna",
				"database": {
					"content": [
						"### Återställ från databasen"
					]
				},
				"json": {
					"content": [
						"### Återställ med en JSON-fil"
					],
					"choose_file": "Välj en fil"
				}
			}
		},
		"contact": {
			"title": "Kontakta oss"
		}
	},
	"docs": {
		"nav": {
			"introduction": "Introduktion",
			"whoweare": "Vilka vi är",
			"install": "Installera",
			"howitworks": "Hur det fungerar"
		},
		"introduction": {
			"title": "Välkommen till dokumentationen för Serenum",
			"content": [
				"Den här dokumentationen lär dig hur Serenum och Serenum API fungerar, vilka vi är och hur du installerar Serenum och/eller Serenum API på din egna server.",
				"### Skillnaden mellan Serenum och andra vädertjänster",
				"- **[SMHI](https://www.smhi.se/en/about-smhi/smhi-s-management-of-personal-data-1.137003)** har ingen integritetspolicy vad vi kunde hitta. Istället har de en sida som berättar hur de behandlar den personlig information som de hämtar från de som använder deras tjänst. Baserat på vad andra lagrar, så antar vi att SMHI lagrar bland annat dina positioner och enhetens IP-adress.\n- **[AccuWeather](https://www.accuweather.com/topic/privacy)** lagrar bland annat detta om dig:\n  - GPS-position\n  - IP-adress\n  - Enhetsinformation (enhets-ID och reklam-ID)\n  - Andra enheter som är nära dig\n  - Sensorerna i din enhet\n  - Enhetens prestanda\n  - Pixel Tags och [Web Beacons](https://en.wikipedia.org/wiki/Web_beacon)\n- **[Serenum](https://serenum.org)** lagrar följande om dig:\n  - Anonym information om vad du har gjort på webbsidan för felsökning",
				"### Vår vision",
				"Vi tror på en värld där alla har rätten till att inte bli spårade över huvud taget. Ditt privatliv ska endast vara **ditt** privatliv och ingen annans. **Du** ska ha rätten till att välja vad för information **du** vill dela till andra. Vi på Serenum bryr oss inte ett smack var i världen du befinner dig, vad för telefon du använder, vad du gör ute på internet, eller något annat.",
				"Ditt liv är ditt liv och inte vårt."
			]
		},
		"whoweare": {
			"title": "Vilka vi är",
			"content": [
				"För närvarande arbetar endast en person på både Serenum och Serenum API: [Erik Edgren](https://airikr.me)."
			]
		},
		"install": {
			"title": "Installation",
			"content": [
				"### Krav",
				"- En webbserver med antingen Apache eller nginx, PHP och (om du vill) MySQL (PDO).\n- 5 minuter av din tid."
			]
		},
		"howitworks": {
			"serenum": {
				"title": "Serenum",
				"content": [
					"- Gömda funktioner\n- Tips & trix"
				]
			},
			"serenumapi": {
				"title": "Serenum API",
				"content": [
					"asd"
				]
			}
		}
	}
}
