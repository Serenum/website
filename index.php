<?php

	require_once 'site-header.php';

	logger('Loaded the intro/about page.');







	echo '<section id="start">';
		echo '<h1>'.$lang->pages->start->title.'</h1>';

		foreach($lang->pages->start->content AS $content) {
			echo (in_array(substr($content, 0, 2), $arr_beginswith) ? $content : $Parsedown->text($content));
		}

		echo '<div class="button">';
			echo '<a href="'.url('weather?pag=now', false).'">';
				echo $lang->pages->start->button;
			echo '</a>';
		echo '</div>';
	echo '</section>';







	require_once 'site-footer.php';

?>
