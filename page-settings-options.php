<?php

	echo '<details>';
		echo '<summary>';
			echo $lang->titles->location_settings;
		echo '</summary>';



		echo '<form action="'.url('forms/settings-options.php', true).'" method="POST" autocomplete="off">';

			# Place
			echo '<div id="place">';

				echo '<h3>';
					echo $lang->titles->place;
				echo '</h3>';

				echo '<div class="desc">';
					echo $lang->settings->place->description;
				echo '</div>';


				# Map (requires JavaScript)
				echo '<div id="map" ';
				echo 'title="'.$lang->map->marker->click.'" ';
				echo 'data-fullscreen-open="'.$lang->map->fullscreen->open.'" ';
				echo 'data-fullscreen-close="'.$lang->map->fullscreen->close.'" ';
				echo 'data-zoom-in="'.$lang->map->zoom->in.'" ';
				echo 'data-zoom-out="'.$lang->map->zoom->out.'" ';
				echo 'data-marker-drag="'.$lang->map->marker->drag.'"';
				echo '></div>';

				# GPS (requires JavaScript)
				echo '<div class="gps">';
					echo '<div class="start"><div>';
						echo '<div class="icon option gps-off"></div>';
						echo '<a href="javascript:void(0)" class="start">';
							echo $lang->settings->gps->start;
						echo '</a>';
					echo '</div></div>';

					echo '<div class="fetching"><div>';
						echo '<div class="icon option gps-searching"></div>';
						echo '<div class="color-blue">'.$lang->settings->gps->fetching.'</div>';
					echo '</div></div>';

					echo '<div class="error permission-denied"><div>';
						echo '<div class="icon option gps-error"></div>';
						echo '<div class="color-red">'.$lang->settings->gps->errors->permission_denied.'.</div>';
						echo '<a href="javascript:void(0)" class="try-again">';
							echo $lang->settings->gps->try_again;
						echo '</a>';
					echo '</div></div>';

					echo '<div class="error position-unavailable"><div>';
						echo '<div class="icon option gps-error"></div>';
						echo '<div class="color-red">'.$lang->settings->gps->errors->position_unavailable.'.</div>';
						echo '<a href="javascript:void(0)" class="try-again">';
							echo $lang->settings->gps->try_again;
						echo '</a>';
					echo '</div></div>';

					echo '<div class="error timeout"><div>';
						echo '<div class="icon option gps-error"></div>';
						echo '<div class="color-red">'.$lang->settings->gps->errors->timeout.'.</div>';
						echo '<a href="javascript:void(0)" class="try-again">';
							echo $lang->settings->gps->try_again;
						echo '</a>';
					echo '</div></div>';

					echo '<div class="found"><div>';
						echo '<div class="icon option gps-found"></div>';
						echo '<a href="javascript:void(0)" class="found">';
							echo $lang->settings->gps->found;
						echo '</a>';
					echo '</div></div>';

					echo '<div class="desc">'.$lang->settings->gps->desc.'</div>';
				echo '</div>';

				# Coordinates
				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang->settings->coordinates.':';
					echo '</div>';

					echo '<div class="field">';
						echo '<input type="text" name="field-coordinates" placeholder="'.$lang->placeholders->required.'" value="'.$get_coordinates.'" tabindex="1">';
					echo '</div>';
				echo '</div>';

			echo '</div>';



			# API keys
			echo '<div id="apikeys">';

				# OpenWeatherMap
				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang->settings->apikeys->owm.':';
					echo '</div>';

					echo '<div class="field">';
						echo '<input type="text" name="field-apikey" placeholder="'.$lang->words->example.': '.md5('this-is-a-fake-apikey').'"';
						echo ((isset($_GET['owm']) AND $get_apikey != $default->apikey) ? ' value="'.$get_apikey.'"' : '');
						echo ' tabindex="2" >';
					echo '</div>';
				echo '</div>';

			echo '</div>';



			# Options
			echo '<div id="options">';

				echo '<h3>';
					echo $lang->titles->settings;
				echo '</h3>';

				# Language
				echo '<div class="item">';
					echo '<div class="label">';
						echo '<div class="icon option language"></div>';
						echo $lang->settings->options->language.':';
					echo '</div>';

					echo '<div class="field">';
						echo '<select name="list-languages" tabindex="3">';
							$c_languages = 0;
							foreach(glob('languages/*.json') AS $language) {
								$c_languages++;
								$langname = json_decode(file_get_contents($language), false);
								$fileinfo = pathinfo($language);

								echo '<option value="'.$fileinfo['filename'].'"';
								echo (isset($_GET['lan']) ? ($get_language == $fileinfo['filename'] ? ' selected' : '') : ($fileinfo['filename'] == 'en' ? ' selected' : ''));
								echo '>'.$langname->metadata->name.'</option>';
							}
						echo '</select>';
					echo '</div>';
				echo '</div>';

				# Temperature
				echo '<div class="item">';
					echo '<div class="label">';
						echo '<div class="icon option temperature"></div>';
						echo $lang->settings->options->temperature.':';
					echo '</div>';

					echo '<div class="value">';
						echo '<input type="radio" name="temperature" id="temp-c" value="0" tabindex="4" ';
						echo (isset($_GET['tem']) ? ($get_temperature == 0 ? ' checked' : '') : ($default->settings->temperature == 0 ? ' checked' : ''));
						echo '>';
						echo '<label for="temp-c">° C</label>';

						echo '<input type="radio" name="temperature" id="temp-f" value="1" tabindex="4" ';
						echo (isset($_GET['tem']) ? ($get_temperature == 1 ? ' checked' : '') : ($default->settings->temperature == 1 ? ' checked' : ''));
						echo '>';
						echo '<label for="temp-f">° F</label>';

						echo '<input type="radio" name="temperature" id="temp-k" value="2" tabindex="4" ';
						echo (isset($_GET['tem']) ? ($get_temperature == 2 ? ' checked' : '') : ($default->settings->temperature == 2 ? ' checked' : ''));
						echo '>';
						echo '<label for="temp-k">° K</label>';
					echo '</div>';
				echo '</div>';


				# Wind speed
				echo '<div class="item">';
					echo '<div class="label">';
						echo '<div class="icon option windspeed"></div>';
						echo $lang->settings->options->windspeed.':';
					echo '</div>';

					echo '<div class="value">';
						echo '<input type="radio" name="windspeed" id="wind-ms" value="0" tabindex="5" ';
						echo (isset($_GET['wis']) ? ($get_windspeed == 0 ? ' checked' : '') : ($default->settings->windspeed == 0 ? ' checked' : ''));
						echo '>';
						echo '<label for="wind-ms">m/s</label>';

						echo '<input type="radio" name="windspeed" id="wind-kmh" value="1" tabindex="5" ';
						echo (isset($_GET['wis']) ? ($get_windspeed == 1 ? ' checked' : '') : ($default->settings->windspeed == 1 ? ' checked' : ''));
						echo '>';
						echo '<label for="wind-kmh">km/h</label>';

						echo '<input type="radio" name="windspeed" id="wind-mph" value="2" tabindex="5" ';
						echo (isset($_GET['wis']) ? ($get_windspeed == 2 ? ' checked' : '') : ($default->settings->windspeed == 2 ? ' checked' : ''));
						echo '>';
						echo '<label for="wind-mph">mph</label>';

						echo '<input type="radio" name="windspeed" id="wind-knot" value="3" tabindex="5" ';
						echo (isset($_GET['wis']) ? ($get_windspeed == 3 ? ' checked' : '') : ($default->settings->windspeed == 3 ? ' checked' : ''));
						echo '>';
						echo '<label for="wind-knot">kt</label>';
					echo '</div>';
				echo '</div>';


				# Pressure
				echo '<div class="item">';
					echo '<div class="label">';
						echo '<div class="icon option airpressure"></div>';
						echo $lang->settings->options->pressure.':';
					echo '</div>';

					echo '<div class="value">';
						echo '<input type="radio" name="pressure" id="pressure-hpa" value="0" tabindex="6" ';
						echo (isset($_GET['prs']) ? ($get_pressure == 0 ? ' checked' : '') : ($default->settings->pressure == 0 ? ' checked' : ''));
						echo '>';
						echo '<label for="pressure-hpa">hPa</label>';

						echo '<input type="radio" name="pressure" id="pressure-pa" value="1" tabindex="6" ';
						echo (isset($_GET['prs']) ? ($get_pressure == 1 ? ' checked' : '') : ($default->settings->pressure == 1 ? ' checked' : ''));
						echo '>';
						echo '<label for="pressure-pa">Pa</label>';

						echo '<input type="radio" name="pressure" id="pressure-bar" value="2" tabindex="6" ';
						echo (isset($_GET['prs']) ? ($get_pressure == 2 ? ' checked' : '') : ($default->settings->pressure == 2 ? ' checked' : ''));
						echo '>';
						echo '<label for="pressure-bar">bar</label>';

						echo '<input type="radio" name="pressure" id="pressure-mmhg" value="3" tabindex="6" ';
						echo (isset($_GET['prs']) ? ($get_pressure == 3 ? ' checked' : '') : ($default->settings->pressure == 3 ? ' checked' : ''));
						echo '>';
						echo '<label for="pressure-mmhg">mmHg</label>';

						echo '<input type="radio" name="pressure" id="pressure-inhg" value="4" tabindex="6" ';
						echo (isset($_GET['prs']) ? ($get_pressure == 4 ? ' checked' : '') : ($default->settings->pressure == 4 ? ' checked' : ''));
						echo '>';
						echo '<label for="pressure-inhg">inHg</label>';

						echo '<input type="radio" name="pressure" id="pressure-psi" value="5" tabindex="6" ';
						echo (isset($_GET['prs']) ? ($get_pressure == 5 ? ' checked' : '') : ($default->settings->pressure == 5 ? ' checked' : ''));
						echo '>';
						echo '<label for="pressure-psi">psi</label>';

						echo '<input type="radio" name="pressure" id="pressure-atm" value="6" tabindex="6" ';
						echo (isset($_GET['prs']) ? ($get_pressure == 6 ? ' checked' : '') : ($default->settings->pressure == 6 ? ' checked' : ''));
						echo '>';
						echo '<label for="pressure-atm">atm</label>';
					echo '</div>';
				echo '</div>';


				# Date format
				echo '<div class="item">';
					echo '<div class="label">';
						echo '<div class="icon option format-date"></div>';
						echo $lang->settings->options->dateformat.':';
					echo '</div>';

					echo '<div class="value">';
						echo '<input type="radio" name="dateformat" id="date-mdy" value="0" tabindex="7" ';
						echo (isset($_GET['dat']) ? ($get_dateformat == 0 ? ' checked' : '') : ($default->settings->dateformat == 0 ? ' checked' : ''));
						echo '>';
						echo '<label for="date-mdy">MMMM DD YYYY</label>';

						echo '<input type="radio" name="dateformat" id="date-dmy" value="1" tabindex="7" ';
						echo (isset($_GET['dat']) ? ($get_dateformat == 1 ? ' checked' : '') : ($default->settings->dateformat == 1 ? ' checked' : ''));
						echo '>';
						echo '<label for="date-dmy">DD MMMM YYYY</label>';
					echo '</div>';
				echo '</div>';


				# Time format
				echo '<div class="item">';
					echo '<div class="label">';
						echo '<div class="icon option format-time"></div>';
						echo $lang->settings->options->timeformat.':';
					echo '</div>';

					echo '<div class="value">';
						echo '<input type="radio" name="timeformat" id="time-12h" value="0" tabindex="8" ';
						echo (isset($_GET['tim']) ? ($get_timeformat == 0 ? ' checked' : '') : ($default->settings->timeformat == 0 ? ' checked' : ''));
						echo '>';
						echo '<label for="time-12h">12h</label>';

						echo '<input type="radio" name="timeformat" id="time-24h" value="1" tabindex="8" ';
						echo (isset($_GET['tim']) ? ($get_timeformat == 1 ? ' checked' : '') : ($default->settings->timeformat == 1 ? ' checked' : ''));
						echo '>';
						echo '<label for="time-24h">24h</label>';
					echo '</div>';
				echo '</div>';


				# Day suffix
				echo '<div class="item">';
					echo '<div class="label">';
						echo '<div class="icon option daysuffix"></div>';
						echo $lang->settings->options->daysuffix->name.':';
					echo '</div>';

					echo '<div class="value">';
						echo '<input type="radio" name="daysuffix" id="daysuffix-show" value="0" tabindex="9" ';
						echo (isset($_GET['dsu']) ? ($get_daysuffix == 0 ? ' checked' : '') : ($default->settings->daysuffix == 0 ? ' checked' : ''));
						echo '>';
						echo '<label for="daysuffix-show">'.$lang->settings->options->daysuffix->show.'</label>';

						echo '<input type="radio" name="daysuffix" id="daysuffix-hide" value="1" tabindex="9" ';
						echo (isset($_GET['dsu']) ? ($get_daysuffix == 1 ? ' checked' : '') : ($default->settings->daysuffix == 1 ? ' checked' : ''));
						echo '>';
						echo '<label for="daysuffix-hide">'.$lang->settings->options->daysuffix->hide.'</label>';
					echo '</div>';
				echo '</div>';


				# Distance
				echo '<div class="item">';
					echo '<div class="label">';
						echo '<div class="icon option distance"></div>';
						echo $lang->settings->options->distance.':';
					echo '</div>';

					echo '<div class="value">';
						echo '<input type="radio" name="distance" id="distance-m" value="0" tabindex="10" ';
						echo (isset($_GET['dis']) ? ($get_distance == 0 ? ' checked' : '') : ($default->settings->distance == 0 ? ' checked' : ''));
						echo '>';
						echo '<label for="distance-m">m</label>';

						echo '<input type="radio" name="distance" id="distance-km" value="1" tabindex="10" ';
						echo (isset($_GET['dis']) ? ($get_distance == 1 ? ' checked' : '') : ($default->settings->distance == 1 ? ' checked' : ''));
						echo '>';
						echo '<label for="distance-km">km</label>';

						echo '<input type="radio" name="distance" id="distance-mi" value="2" tabindex="10" ';
						echo (isset($_GET['dis']) ? ($get_distance == 2 ? ' checked' : '') : ($default->settings->distance == 2 ? ' checked' : ''));
						echo '>';
						echo '<label for="distance-mi">mi</label>';
					echo '</div>';
				echo '</div>';


				# Precipitation
				echo '<div class="item">';
					echo '<div class="label">';
						echo '<div class="icon option precipitation-mix"></div>';
						echo $lang->settings->options->precipitation.':';
					echo '</div>';

					echo '<div class="value">';
						echo '<input type="radio" name="precipitation" id="precip-mm" value="0" tabindex="11" ';
						echo (isset($_GET['prc']) ? ($get_precipitation == 0 ? ' checked' : '') : ($default->settings->precipitation == 0 ? ' checked' : ''));
						echo '>';
						echo '<label for="precip-mm">mm</label>';

						echo '<input type="radio" name="precipitation" id="precip-cm" value="1" tabindex="11" ';
						echo (isset($_GET['prc']) ? ($get_precipitation == 1 ? ' checked' : '') : ($default->settings->precipitation == 1 ? ' checked' : ''));
						echo '>';
						echo '<label for="precip-cm">cm</label>';

						echo '<input type="radio" name="precipitation" id="precip-in" value="2" tabindex="11" ';
						echo (isset($_GET['prc']) ? ($get_precipitation == 2 ? ' checked' : '') : ($default->settings->precipitation == 2 ? ' checked' : ''));
						echo '>';
						echo '<label for="precip-in">in</label>';
					echo '</div>';
				echo '</div>';


				# Seperator
				echo '<div class="item">';
					echo '<div class="label">';
						echo '<div class="icon option decimal"></div>';
						echo $lang->settings->options->decimal.':';
					echo '</div>';

					echo '<div class="value">';
						echo '<input type="radio" name="decimal" id="decimal-comma" value="0" tabindex="12" ';
						echo (isset($_GET['dec']) ? ($get_decimal == 0 ? ' checked' : '') : ($default->settings->decimal == 0 ? ' checked' : ''));
						echo '>';
						echo '<label for="decimal-comma">'.$lang->words->comma.' (,)</label>';

						echo '<input type="radio" name="decimal" id="decimal-dot" value="1" tabindex="12" ';
						echo (isset($_GET['dec']) ? ($get_decimal == 1 ? ' checked' : '') : ($default->settings->decimal == 1 ? ' checked' : ''));
						echo '>';
						echo '<label for="decimal-dot">'.$lang->words->dot.' (.)</label>';
					echo '</div>';
				echo '</div>';


				# Split thousands
				echo '<div class="item">';
					echo '<div class="label">';
						echo '<div class="icon option split-thousands"></div>';
						echo $lang->settings->options->split_thousands.':';
					echo '</div>';

					echo '<div class="value">';
						echo '<input type="radio" name="split-thousands" id="split-thousands-yes" value="0" tabindex="13" ';
						echo (isset($_GET['ths']) ? ($get_splitthousands == 0 ? ' checked' : '') : ($default->settings->split_thousands == 0 ? ' checked' : ''));
						echo '>';
						echo '<label for="split-thousands-yes">'.$lang->words->yes.'</label>';

						echo '<input type="radio" name="split-thousands" id="split-thousands-no" value="1" tabindex="13" ';
						echo (isset($_GET['ths']) ? ($get_splitthousands == 1 ? ' checked' : '') : ($default->settings->split_thousands == 1 ? ' checked' : ''));
						echo '>';
						echo '<label for="split-thousands-no">'.$lang->words->no.'</label>';
					echo '</div>';
				echo '</div>';


				# Theme
				echo '<div class="item">';
					echo '<div class="label">';
						echo '<div class="icon option theme"></div>';
						echo $lang->settings->options->theme.':';
					echo '</div>';

					echo '<div class="value">';
						echo '<input type="radio" name="theme" id="theme-auto" value="0" tabindex="14" ';
						echo (isset($_GET['thm']) ? ($get_theme == 0 ? ' checked' : '') : ($default->settings->theme == 0 ? ' checked' : ''));
						echo '>';
						echo '<label for="theme-auto">'.$lang->words->auto.'</label>';

						echo '<input type="radio" name="theme" id="theme-light" value="1" tabindex="14" ';
						echo (isset($_GET['thm']) ? ($get_theme == 1 ? ' checked' : '') : ($default->settings->theme == 1 ? ' checked' : ''));
						echo '>';
						echo '<label for="theme-light">'.$lang->words->light.'</label>';

						echo '<input type="radio" name="theme" id="theme-dark" value="2" tabindex="14" ';
						echo (isset($_GET['thm']) ? ($get_theme == 2 ? ' checked' : '') : ($default->settings->theme == 2 ? ' checked' : ''));
						echo '>';
						echo '<label for="theme-dark">'.$lang->words->dark.'</label>';
					echo '</div>';
				echo '</div>';

			echo '</div>';



			echo '<div class="button">';
				echo '<input type="submit" name="button-ok" value="'.$lang->settings->button_save.'" tabindex="15">';
			echo '</div>';
		echo '</form>';
	echo '</details>';

?>
