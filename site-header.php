<?php

	require_once 'site-settings.php';



	echo '<!DOCTYPE html>';
	echo '<html>';
		echo '<head>';
			echo '<title>';
				echo $og_title;
			echo '</title>';

			echo '<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">';
			echo '<meta name="robots" content="noindex, nofollow">';
			echo '<meta name="google" content="notranslate">';

			echo '<meta property="og:title" content="'.$og_title.'">';
			echo '<meta property="og:site_name" content="'.$og_title.'">';
			echo '<meta property="og:type" content="website">';
			echo '<meta property="og:url" content="'.(isset($og_url) ? '-' : $og_url).'">';
			echo '<meta property="og:description" content="'.($og_description == null ? '-' : $og_description).'">';
			echo '<meta property="og:locale" content="sv_SE">';
			echo ($filename == 'page-show-weather.php' ? '<meta http-equiv="refresh" content="3600">' : null);

			echo '<meta name="twitter:card" content="summary"></meta>';

			echo '<link rel="canonical" href="'.(isset($og_url) ? '-' : $og_url).'">';
			echo (($config->minify->enabled == true AND $config->minify->use_cdn == true) ? '<link rel="dns-prefetch" href="'.$config->minify->cdn_url.'" crossorigin="">' : '');

			echo '<link href="'.($config->minify->enabled == true ? ($config->minify->use_cdn == true ? $config->minify->cdn_url.'/img/favicon.ico' : url($path_images.'/favicon.ico')) : $path_images.'/favicon.ico').'" rel="icon" type="image/x-icon">';
			#echo '<link href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQEAYAAABPYyMiAAAABmJLR0T///////8JWPfcAAAACXBIWXMAAABIAAAASABGyWs+AAAAF0lEQVRIx2NgGAWjYBSMglEwCkbBSAcACBAAAeaR9cIAAAAASUVORK5CYII=" rel="icon" type="image/x-icon">';
			echo '<link rel="manifest" href="'.url('manifest.json').'">';
			echo '<link rel="manifest" href="'.url('site.webmanifest').'">';

			if($config->minify->enabled == true) {
				echo '<link type="text/css" rel="stylesheet" href="'.($config->minify->use_cdn == true ? $config->minify->cdn_url.'/css/theme-'.($get_theme == 0 ? 'auto' : ($get_theme == 1 ? 'light' : 'dark')).'.min.css' : url('css/theme-'.($get_theme == 0 ? 'auto' : ($get_theme == 1 ? 'light' : 'dark')).'.min.css')).'">';
				echo '<link type="text/css" rel="stylesheet" href="'.($config->minify->use_cdn == true ? $config->minify->cdn_url.'/css/style.min.css' : url('css/style.min.css')).'">';
				echo '<link type="text/css" rel="stylesheet" href="'.($config->minify->use_cdn == true ? $config->minify->cdn_url.'/css/pe-icon-7-weather.min.css' : url('css/pe-icon-7-weather.min.css')).'">';
				echo '<link type="text/css" rel="stylesheet" href="'.($config->minify->use_cdn == true ? $config->minify->cdn_url.'/css/weather-icons.min.css' : url('css/weather-icons.min.css')).'">';
				echo '<link type="text/css" rel="stylesheet" href="'.($config->minify->use_cdn == true ? $config->minify->cdn_url.'/css/leaflet.min.css' : url('css/leaflet.min.css')).'">';

				echo '<script type="text/javascript" src="'.($config->minify->use_cdn == true ? $config->minify->cdn_url.'/js/jquery-slim.min.js' : url('js/jquery-slim.min.js')).'"></script>';
				echo '<script type="text/javascript" src="'.($config->minify->use_cdn == true ? $config->minify->cdn_url.'/js/mousetrap.min.js' : url('js/mousetrap.min.js')).'"></script>';
				#echo '<script type="text/javascript" src="'.($config->minify->use_cdn == true ? $config->minify->cdn_url.'/js/swipe-events.min.js' : url('js/swipe-events.min.js')).'"></script>';
				echo '<script type="text/javascript" src="'.($config->minify->use_cdn == true ? $config->minify->cdn_url.'/js/leaflet.min.js' : url('js/leaflet.min.js')).'"></script>';
				echo '<script type="text/javascript" src="'.($config->minify->use_cdn == true ? $config->minify->cdn_url.'/js/main.min.js' : url('js/main.min.js')).'"></script>';

			} else {
				echo '<link type="text/css" rel="stylesheet" href="'.url('css/theme-'.($get_theme == 0 ? 'auto' : ($get_theme == 1 ? 'light' : 'dark')).'.css?'.time()).'">';
				echo '<link type="text/css" rel="stylesheet" href="'.url('css/desktop.css?'.time()).'">';
				echo '<link type="text/css" rel="stylesheet" href="'.url('css/portable.css?'.time()).'">';
				echo '<link type="text/css" rel="stylesheet" href="'.url('css/pe-icon-7-weather.css?'.time()).'">';
				echo '<link type="text/css" rel="stylesheet" href="'.url('css/weather-icons.min.css?'.time()).'">';
				echo '<link type="text/css" rel="stylesheet" href="'.url('css/weather-icons-wind.css?'.time()).'">';
				echo '<link type="text/css" rel="stylesheet" href="'.url('css/leaflet.min.css?'.time()).'">';

				echo '<script type="text/javascript" src="'.url('js/jquery-slim.min.js').'"></script>';
				echo '<script type="text/javascript" src="'.url('js/mousetrap.min.js').'"></script>';
				#echo '<script type="text/javascript" src="'.url('js/swipe-events.min.js').'"></script>';
				echo '<script type="text/javascript" src="'.url('js/leaflet.min.js').'"></script>';
				echo '<script type="text/javascript" src="'.url('js/main.js?'.time()).'"></script>';
			}
		echo '</head>';
		echo '<body>';



			echo '<noscript>';
				echo '<style type="text/css">';
					echo 'div#map, .gps, div#place > .desc { display: none !important; }';
				echo '</style>';
			echo '</noscript>';

			echo '<section id="website">';
				echo '<header id="logo">';
					echo '<a href="'.url('weather?pag=now', false).'">';
						echo '<div class="image"></div>';

						/*if($filename == 'page-show-weather.php' OR isset($_GET['coo']) AND $filename == 'page-settings-save.php' OR $filename == 'page-settings-restore.php') {
							echo '<div class="text">';
								echo $coordinate->format($formatter);
							echo '</div>';
						}*/
					echo '</a>';
				echo '</header>';

				echo '<main';
				echo ' data-useminified="'.($config->minify->enabled == true ? 'yes' : 'no').'"';
				echo ' data-usecdn="'.($config->minify->use_cdn == true ? 'yes' : 'no').'"';
				echo ' data-cdnurl="'.$config->minify->cdn_url.'"';
				echo '>';

?>
